import hashlib
from datetime import datetime
from ConnessioneDB import db, fs
from Common import *
from pymongo import ASCENDING, DESCENDING

def eliminaUtente(datiU):
	u = getByUsername(datiU["username"])
	db.users.delete_one({'_id': u["_id"]})
	db.usergrp.delete_many({'userid': u["_id"]})
	cur = fs.find({'metadata.owner': u["_id"]})
	for i in cur:
		elim = i._id
		i.close()
		eliminaById(elim)

def checkAdmin(sessione):
	utente = checkSessione(sessione)
	if utente is not None:
		if utente["rank"] == "admin":
			return True
	return False

def checkSessione(sessione):
	utente = db.users.find_one({'session': sessione})
	if sessione is not None:
		if utente is not None and utente["session"] is not None:
			return utente
	return None

def checkPwd(utente, password):
	m = hashlib.sha512()
	m.update(utente["salt"]+password.encode('ascii'))
	res = m.digest()
	if res == utente["password"]:
		return True
	else:
		return False

def nuovoLoginUtente(datiLogin):
	chiaveSessione = None;
	utente = getByUsername(datiLogin["username"])
	if utente and checkPwd(utente, datiLogin["password"]) :
		m = hashlib.sha224()
		ora = datetime.now().strftime("%m/%d/%Y, %H:%M:%S.%f")
		stringa = datiLogin["username"].lower() + ora + datiLogin["username"].upper()
		m.update(stringa.encode('ascii'))
		nuovaSessione = m.hexdigest()
		db.users.update_one({'username': datiLogin["username"]}, \
							{'$set': {'session': nuovaSessione}})
		chiaveSessione = nuovaSessione
	return chiaveSessione

def logoutUtente(sessione):
	res = True
	utente = db.users.find_one({'session': sessione})
	if utente is not None:
		db.users.update_one({'username': utente["username"]}, \
							{'$set': {'session': None}})
	else: 
		res = False
	return res

def promuoviAdmin(username):
	utente = db.users.find_one({'username': username, 'rank': "user"})
	if utente is not None:
		db.users.update_one({'username': utente["username"]}, \
							{'$set': {'rank': "admin"}})
		return True
	return False

def creaUtente(datiSignup):
	if getByUsername(datiSignup['username']) is not None:
		return False
	m = hashlib.sha512()
	datiSignup["rank"]= "user"
	datiSignup["session"] = None
	ora = datetime.now().strftime("%m/%d/%Y, %H:%M:%S.%f")
	datiSignup["salt"] = ora.encode('ascii')
	m.update(datiSignup["salt"]+datiSignup["password"].encode('ascii'))
	datiSignup["password"] = m.digest()
	pub = db.groups.find_one({'groupname': "public"})
	res = db.users.insert_one(datiSignup)
	db.usergrp.insert_one({"userid": res.inserted_id, "groupid": pub['_id']})
	return True

def getListaUtenti():
	lista = []
	cur = db.users.find({}, {'_id': 0, 'username': 1}) \
				.sort('username', ASCENDING)
	for i in cur:
		lista.append(i['username'])
	return lista

def getListaAdmin():
	lista = []
	cur = db.users.find({'rank': "admin"}, {'_id': 0, 'username': 1}) \
				.sort('username', ASCENDING)
	for i in cur:
		lista.append(i['username'])
	return lista

