from flask import Flask, request, send_file, make_response
from flask_cors import CORS
import json, sys, io

sys.path.append(".")
from ProcedureUtenti import *
from ProcedureFile import *
from ProcedureGruppi import *

app = Flask(__name__)
CORS(app)

if __name__ == '__main__':
	app.run(host='0.0.0.0')

@app.route('/utenti', methods = ['POST', 'DELETE'])
def gestioneUtenti():
	sessione = request.headers.get('session')
	dati = request.get_json(force = True)
	if checkAdmin(sessione):
		if request.method == 'POST':
			if creaUtente(dati):
				return "Eseguito!"
			return "Utente già esistente", 409
		elif request.method == 'DELETE':
			eliminaUtente(dati)
			return "Eseguito!"
	return "Non autorizzato!", 401

@app.post('/login')
def exeloginUtente():
	datiLogin = request.get_json(force = True)
	chiave = nuovoLoginUtente(datiLogin)
	if chiave is not None:
		return json.dumps({"session": chiave})
	return "Errore, utente non esistente", 404

@app.get('/logout')
def exelogoutUtente():
	sessione = request.headers.get('session')
	if checkSessione(sessione):
		logoutUtente(sessione)
		return "Eseguito!"
	return "Errore, nessun login effettuato!", 401
		
@app.post('/amministratori')
def _promuoviAdmin():
	sessione = request.headers.get('session')
	dati = request.get_json(force = True)
	if checkAdmin(sessione):
		if promuoviAdmin(dati["username"]):
			return "Eseguito!"
		else:
			return "Già amministratore", 409
	return "Non autorizzato!", 401
		
@app.get('/lista-utenti')
def listaUtenti():
	sessione = request.headers.get('session')
	if checkSessione(sessione):
		return json.dumps(getListaUtenti())
	return "Non autorizzato!", 401
	
@app.get('/lista-amministratori')
def listaAmministratori():
	sessione = request.headers.get('session')
	if checkSessione(sessione):
		return json.dumps(getListaAdmin())
	return "Non autorizzato!", 401
		
@app.route('/file', methods = ['GET', 'POST', 'DELETE'])
def gestioneFile():
	sessione = request.headers.get('session')
	utente = checkSessione(sessione)
	if utente is not None:
		if request.method == 'GET':
			dati = request.args
			f = None
			if 'owner' not in dati:
				f = getFile(dati['filename'], utente)
			else:
				f = getFileCheck(dati['filename'], utente, dati['owner'])
			if f is not None:
				response = make_response(send_file(io.BytesIO(f.read()), \
					attachment_filename = f.filename, \
					mimetype = 'application/octet-stream'))
				response.headers['Content-Type'] = "application/octet-stream"
				response.headers['Content-Disposition'] = "attachment; filename="+ \
					f.filename +"; filename*="+ f.filename
				response.headers['Content-Length'] = f.length
				#f.close()
				return response
			else:
				return "File inesistente", 404
		if utente["rank"] == "admin":
			if request.method == 'POST':
				if 'file' not in request.files:
					return "Non hai inviato nulla!", 415
				f = request.files['file']
				if f.filename == '':
					return "Non hai selezionato nessun'file!", 415
				if checknome(f.filename):
					creaFile(f, utente)
					return "Eseguito!"
				else:
					return "Nessun'attacco XSS, grazie!", 415
			elif request.method == 'DELETE':
				dati = request.get_json(force = True)
				eliminaFile(dati['filename'], utente)
				return "Eseguito!"
	return "Non autorizzato!", 401

@app.get('/lista-file')
def listaFile():
	sessione = request.headers.get('session')
	utente = checkSessione(sessione)
	if utente is not None:
		dati = request.args
		if utente["rank"] == "admin":
			if(len(dati) == 0):
				return json.dumps(getListFileU(utente))
			return json.dumps( \
			getListFileU(getByUsername(dati["username"])))
		else:
			return json.dumps(getListFileU(utente))
	return "Non autorizzato!", 401

@app.route('/gruppi', methods=['POST', 'DELETE'])
def gestioneGruppi():
	sessione = request.headers.get('session')
	utente = checkSessione(sessione)
	if utente is not None and utente["rank"] == "admin":
		dati = request.get_json(force = True)
		if request.method == 'POST':
			if creaGruppo(dati, utente):
				return "Eseguito!"
			return "Gruppo già esistente", 409
		elif request.method == 'DELETE':
			eliminaGruppo(dati['groupname'])
			return "Eseguito!"
	return "Non autorizzato!", 401

@app.route('/file-gruppi', methods = ['GET', 'POST', 'DELETE'])
def gestisciFileGruppi():
	sessione = request.headers.get('session')
	utente = checkSessione(sessione)
	if utente is not None:
		if utente["rank"] == "admin":
			if request.method == "POST":
				dati = request.get_json(force = True)
				if creaFileGruppo(utente['_id'], dati['filename'], \
				dati['groupname']):
					return "Eseguito!"
				else:
					return "Errore nei dati inseriti", 409
			elif request.method == "DELETE":
				dati = request.get_json(force = True)
				eliminaFileGruppo(utente['_id'], dati['filename'], \
				dati['groupname'])
				return "Eseguito!"
		if request.method == "GET":
			dati = request.args
			return json.dumps(getFilesInGruppo(dati['groupname']))
	return "Non autorizzato!", 401

@app.route('/utenti-gruppi', methods = ['GET', 'POST', 'DELETE'])
def gestisciUtentiGruppi():
	sessione = request.headers.get('session')
	utente = checkSessione(sessione)
	if utente is not None:
		if utente["rank"] == "admin":
			if request.method == "POST":
				dati = request.get_json(force = True)
				if creaUtenteGruppo(dati['username'], dati['groupname']):
					return "Eseguito!"
				else:
					return "Errore nei dati inseriti", 409
			elif request.method == "DELETE":
				dati = request.get_json(force = True)
				eliminaUtenteGruppo(dati['username'], dati['groupname'])
				return "Eseguito!"
		if request.method == "GET":
			if utente['rank'] == "admin":
				dati = request.args
				print(dati['groupname'])
				return json.dumps(getUtentiInGruppo(dati['groupname']))
	return "Non autorizzato!", 401

@app.get('/lista-gruppi')
def listaGruppi():
	sessione = request.headers.get('session')
	utente = checkSessione(sessione)
	if utente is not None:
		dati = request.args
		if utente['rank'] == "admin":
			if len(dati) == 0:
				return json.dumps(getListGruppi())
			else:
				return json.dumps(getListGruppiU(getByUsername(dati['username'])))
		else:
			return json.dumps(getListGruppiU(utente))
	return "Non autorizzato!", 401



