from ConnessioneDB import db, fs
from Common import *
from pymongo import ASCENDING, DESCENDING

def creaGruppo(dati, u):
	if getByGroupname(dati['groupname']) is None:
		ins = db.groups.insert_one({'groupname': dati["groupname"]})
		db.usergrp.insert_one({'userid': u['_id'], 'groupid': ins.inserted_id})
		return True
	return False

def eliminaGruppo(nome):
	g = getByGroupname(nome)
	if g is not None:
		oldId = g['_id']
		db.usergrp.delete_many({'groupid': oldId})
		db.filegrp.delete_many({'groupid': oldId})

def getUtentiInGruppo(groupname):
	g = getByGroupname(groupname)
	lista = []
	if g is not None:
		cur = db.usergrp.find({'groupid': g['_id']})
		for i in cur:
			u = db.users.find_one({'_id': i['userid']})
			lista.append(u['username'])
	return lista

def getFilesInGruppo(groupname):
	g = getByGroupname(groupname)
	lista = []
	if g is not None:
		cur = db.filegrp.find({'groupid': g['_id']})
		for i in cur:
			rec = db.fs.files.find_one({'_id': i['fileid']})
			lista.append({"filename": rec["filename"], "filesize": rec["length"], "owner": rec["metadata"]["owname"]})
	return lista

def creaUtenteGruppo(username, groupname):
	u = getByUsername(username)
	g = getByGroupname(groupname)
	if u is not None and g is not None:
		db.usergrp.insert_one({'userid': u['_id'], 'groupid': g['_id']})
		return True
	return False
	
def creaFileGruppo(userId, filename, groupname):
	g = getByGroupname(groupname)
	f = db.fs.files.find_one({'filename': filename, 'metadata.owner': userId})
	if f is not None and g is not None:
		db.filegrp.insert_one({'fileid': f['_id'], 'groupid': g['_id']})
		return True
	return False
	
def eliminaUtenteGruppo(username, groupname):
	u = getByUsername(username)
	g = getByGroupname(groupname)
	if u is not None and g is not None:
		db.usergrp.delete_one({'userid': u['_id'], 'groupid': g['_id']})

def eliminaFileGruppo(userId, filename, groupname):
	g = getByGroupname(groupname)
	rec = db.fs.files.find_one({'filename': filename, 'metadata.owner': userId})
	if rec is not None and g is not None:
		db.filegrp.delete_one({'fileid': rec["_id"], 'groupid': g["_id"]})

def getListGruppi():
	lista = []
	cur = db.groups.find({}, {'_id': 0})
	for i in cur:
		lista.append(i['groupname'])
	return lista

def getListGruppiU(u):
	cur = db.usergrp.find({'userid': u['_id']})
	lista = []
	for i in cur:
		tmp = db.groups.find_one({'_id': i['groupid']}, \
		{'_id': 0})
		lista.append(tmp['groupname'])
	return lista

def getGruppiFile(fileid):
	cur = db.filegrp.find({'fileid': fileid})
	lista = []
	for i in cur:
		tmp = db.groups.find_one({'_id': i['groupid']}, {'_id': 0})
		lista.append(tmp['groupname'])
	return lista
