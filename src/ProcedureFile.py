from ConnessioneDB import db, fs
from pymongo import ASCENDING, DESCENDING
from Common import *
import os

def getListFile():
	cur = db.fs.files.find({}) \
		.sort([('metadata.owname', ASCENDING), \
		('filename', ASCENDING)])
	lista = []
	for i in cur:
		lista.append({'filename': i["filename"], 'owner': i["metadata"]["owname"], \
			'filesize': i["length"]})
	return lista

def getListFileU(u):
	if u is not None:
		cur = db.fs.files.find({'metadata.owner': u['_id']}) \
		.sort('filename', ASCENDING)
		lista = []
		for i in cur:
			lista.append({'filename': i["filename"], \
			'filesize': i["length"]})
		return lista
	return None

def getFile(filename, u):
	return trovaFile(filename, u['_id'])

def getFileCheck(filename, u, owname):
	f = None
	owner = getByUsername(owname)
	f = getFile(filename, owner)
	return f

def creaFile(f, u):
	elim = trovaFile(f.filename, u["_id"])
	oldId = None
	if elim is not None:
		oldId = elim._id
		elim.close()
		fs.delete(oldId)
		fs.put(f, _id=oldId, filename=f.filename, \
			metadata={'owner': u["_id"], 'owname': u["username"]})
	else:
		recId = fs.put(f, filename=f.filename, metadata={'owner': u["_id"], 'owname': u["username"]})
		pub = getByGroupname("public")
		db.filegrp.insert_one({'fileid': recId, 'groupid': pub['_id']})
		

def eliminaFile(filename, u):
	f = trovaFile(filename, u['_id'])
	delId = f._id
	if f is not None:
		f.close()
		eliminaById(delId)

def trovaFile(filename, owner):
	return fs.find_one({"filename": filename, "metadata.owner": owner})

def checknome(filename):
	noXSS = [".html", ".htm", ".js", ".php", ".jar", ".xap", ".swf", ".xml"]
	estensione = os.path.splitext(filename)[1]
	if estensione not in noXSS:
		return True
	else:
		return False
