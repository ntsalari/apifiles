from ConnessioneDB import db, fs

def getByUsername(username):
	return db.users.find_one({'username': username})
	
def getByGroupname(nome):
	return db.groups.find_one({'groupname': nome})

def eliminaById(_id):
	fs.delete(_id)
	db.filegrp.delete_many({'fileid': _id})
